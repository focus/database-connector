import pandas as pd
import time
from openpyxl import load_workbook
from openpyxl import workbook
import os
import xlsxwriter


price_path = 'timeseries_data/prices.pkl'
irradiance_path = 'timeseries_data/PV_factors.pkl'
temperature_path = 'timeseries_data/temperature.csv'
irradiance_DWD_path = 'timeseries_data/irradiance_stuttgart.pkl'
irradiance_BSRN_path = 'timeseries_data/Lindenberg2006BSRN_Irradiance_60sec.csv'
temperature_BSRN_path = 'timeseries_data/Lindenberg2006_Temperature.csv'




def prepare_price_data(path):  # price data is prepared outside the project and read as a pkl
    prices = pd.read_pickle(path)
    time_start = prices.index[0]
    print(time_start)
    time_end = prices.index[prices.index[:].size-1]
    print(time_end)
    time_resolution = prices.index[1] - prices.index[0]
    print(time_resolution)

def prepare_price_csv_data(path):
    """take the electricity trading price of Germany in 2020 from Nordpool"""
    new_index = pd.date_range(start="1/1/2019", periods=8785, freq="H")
    prices = pd.read_excel(path, skiprows=2)
    prices.index = new_index
    de_prices = prices['DE-LU']
    return de_prices


def prepare_irradiance_data(path):
    irradiance = pd.read_pickle(path)
    return irradiance


def prepare_irradiance_dwd(path):
    irradiance = pd.read_pickle(path)
    # radiation data is given in J/cm^2 -> 1 J/cm^2 = 2.77778 Wh/m^2
    # we should consider this when changing to 15min resolution
    global_irradiance = irradiance['FG_LBERG'] * 2.77778
    return global_irradiance


def prepare_temperature_data(path, t_step):
    import numpy as np
    start_date = pd.Timestamp("2019-01-01 00:00:00")
    time_horizon = 8760
    time_steps = []
    for t in pd.date_range(start_date,
                           pd.Timestamp("2020-01-01 00:00:00"),#start_date + timedelta(hours=time_horizon - 1),
                           freq='H'):
        time_steps.append(t)
    # important: only pass the temperature column, not the steps
    temperature = pd.read_csv(path)['temperature']

    temperature_df = pd.Series(data=np.array(temperature),
                               index=pd.Index(time_steps))

    temp_15 = temperature_df.resample(str(t_step)+'H').interpolate(method='polynomial', order=5)[pd.Timestamp("2019-01-01 00:00:00"):pd.Timestamp("2019-12-31 23:45:00")]
    return temp_15


def prepare_temperature_bsrn(path, t_step):
    import numpy as np
    start_date = "2019-01-01 00:00:00"
    end_date = "2019-12-31 23:59:00"
    time_steps = []
    for t in pd.date_range(start_date, end_date,
                           freq='60S'):
        time_steps.append(t)
    # important: only pass the temperature column, not the steps
    df = pd.read_csv(path)

    # data actually starts one hour earlier:
    main_part = df[:(60 * 8760 - 60)]
    # print(len(main_part))
    extra_hour = df[:60]
    # print(extra_hour)
    df = pd.concat([extra_hour, main_part])

    df_temp = pd.DataFrame(data=np.array(df), index=time_steps)
    temperature = df_temp.resample(str(t_step)+'H').mean()
    print(temperature[0])
    #print(temperature.isna().values.any())
    return temperature[0]

def prepare_bsrn_data(path, t_step):
    import numpy as np
    start_date = "2019-01-01 00:00:00"
    end_date = "2019-12-31 23:59:00"
    time_steps = []
    for t in pd.date_range(start_date, end_date,
                           freq='60S'):
        time_steps.append(t)
    # important: only pass the temperature column, not the steps
    df = pd.read_csv(path)

    #data actually starts one hour earlier:
    main_part=df[:(60*8760-60)]
    #print(len(main_part))
    extra_hour=df[:60]
    #print(extra_hour)
    df = pd.concat([extra_hour, main_part])

    df_irr = pd.DataFrame(data=np.array(df), index=time_steps)
    print(df_irr.max())
    irradiance = df_irr.resample(str(t_step)+'H').mean()
    print(irradiance.max())
    print(irradiance)
    return irradiance


#read csv
"""
This function aims to get .pkl or .csv file and uploads all data from it.
First data has to be formed in the way the excel file is. So generate column: 
id, data_id, value, time_start, time_end, time_resolution
 """

cwd = os.getcwd()
data_path = price_path
excel_name = 'struktur_neu.xlsx'
filename, file_extension = os.path.splitext(data_path)

keynames = ['id', 'data_id', 'value', 'time_start', 'time_end', 'time_resolution']
upload_dict = dict.fromkeys(keynames)

if file_extension == '.csv':
    input_data = pd.read_csv(data_path)
elif file_extension == '.pkl':
    input_data = pd.read_pickle(data_path)

upload_dict['id'] = 1
upload_dict['data_id'] = 39
upload_dict['value'] = input_data.to_json(orient="records")
upload_dict['time_start'] = '2019-07-03 22:00:00'
upload_dict['time_end'] = '2020-08-15 22:00:00'
upload_dict['time_resolution'] = 60


if file_extension == '.csv':

    input_data = pd.read_csv(data_path)

    start_loop = time.perf_counter()
    # write into oep-float-string
    oep_string = '{' + str(input_data.loc[0][0])

    for num in range(1, len(input_data)):
        number = str(input_data.loc[num][0])
        oep_string = ', ' + oep_string + number
    oep_string = oep_string + '}'
    end_loop = time.perf_counter()
    total_looptime = end_loop - start_loop

elif file_extension == '.pkl':
    input_data = pd.read_pickle(data_path)

    start_loop = time.perf_counter()
    #write into oep-float-string
    oep_string = '{'

    for num in range(len(input_data)):
        number = str(input_data[num])
        if num < len(input_data)-1:
            oep_string = oep_string + number + ', '
        else:
            oep_string = oep_string + number

    oep_string = oep_string + '}'
    end_loop = time.perf_counter()
    total_looptime = end_loop - start_loop
    print(total_looptime)
#input_data = pd.DataFrame(data=[1, 2, 3, 4])




"""---------openpyxl-----------------"""
fn = os.path.join(cwd, excel_name)
wb = load_workbook(fn)
sheet = wb['timeseries']

sheet.cell(row=3, column=3).value = oep_string
wb.save(fn)


"""------------Pandas-------------------"""
"""
#write to excel
fn = os.path.join(cwd, drain)

df = pd.read_excel(fn, header=None, sheet_name='timeseries', engine='openpyxl')
df2 = input_data

writer = pd.ExcelWriter(fn, engine='openpyxl')
book = load_workbook(fn)
writer.book = book
writer.sheets = dict((ws.title, ws) for ws in book.worksheets)

df.to_excel(writer, sheet_name='timeseries', header=None, index=False)
df2.to_excel(writer, sheet_name='timeseries', header=None, index=False,
             startcol=3,startrow=2)

writer.save()
"""



"""------------xlsxwriter--------------"""
"""
workbook = xlsxwriter.Workbook(drain)
worksheet = workbook('timeseries')

worksheet.write_string('C2', oep_array)

print(oep_array)
"""