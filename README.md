# InEEd-DC Database Connector

InEEd-DC Database Connector - **!! SHORT DESCRIPTION TO BE DONE !!**

## Project Structure
**!! TO BE DONE !!**

## Contribution Guidelines
If you want to contribute to the InEEd-DC Database Connector, please follow the rules and guidelines specified below:
### Branches
- All developing works should be done in personal branchs or feature branches. Please follow the name guidelines for creating new branches: `dev_<NameAbbr>` or `dev_<feature>`.
- Merge requests to the `main` branch should be coordinated with other developers in advance.
- The main branch will be updated at least every two weeks.

### Merge into main branch
For the update process (mergen) of main branch, the following instruction in 7 Steps has been presented and should be followed:
| Step | discription |
| ------ | ------ |
| Step 1 | pull main branch (remote) |
| Step 2 | merge main branch (local) in MyFeature branch (local) and solve merge-conflicts in MyFeature branch |
| Step 3 | test the MyFeature branch (local) by its functionality |
| Step 4 | merge MyFeature branch (local) in main branch (local) |
| Step 5 | test the main branch (local) by its functionality |
| Step 6 | commit MyFeature branch (local) and push MyFeature branch to remote |
| Step 7 | submit a merge request from MyFeature branch (remote) to main branch (remote) |

Importance:
 - On principle, only functional codes can be merged. If this is not the case, put WIP (Work in progress) in the title of merge request.
 - Choose "Squash Commit" in the merge request so that the commit history will be set invisible.
 - Always use merge request to update work, so that the working history is tracable.

### Environment File
- The environment file in the `main` branch should always be synchronized with the program and should be updated simultaneously with other files.
- In order to keep the used third-party packages in a reasonable size, it is necessary to stay conservative when adding new packages.

### Modeling Language
The currently used modeling language is:
> pyomo: https://pyomo.readthedocs.io/en/stable/index.html

### Programming Guidelines
**!!To be done!!**
A comprehensive discription on how new attributes/variables/constraints/objectives/sectors/components/prosumers/.. can be added and edited.

### Coding-Style
- This project follows the Google Python Style Guide:
> Google Python Style Guide: https://google.github.io/styleguide/pyguide.html#s2.6-nested
