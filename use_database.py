from oem2orm import oep_oedialect_oem2orm as oem2orm
import os
import pathlib
import requests
import pandas as pd
from openpyxl import load_workbook
import numpy as np


def get_excel_data(path):
    all_excel_dict = pd.read_excel(path, sheet_name=None, engine='openpyxl')
    clean_excel_dict = clean_excel(all_excel_dict)

    return clean_excel_dict


def clean_excel(excel_dict):
    # ToDo: right now, every column and row needs to have at least one datapoint to not get deleted by this function
    for item in excel_dict.items():
        df_name = item[0]
        df = item[1]
        # delete rows/columns with just nan
        df.dropna(axis=1, how='all', inplace=True)
        df.dropna(axis=0, how='all', inplace=True)

        # fill other nan's
        df = df.fillna('')

        # change df in dict
        excel_dict[df_name] = df

    return excel_dict


def upload_excel_data(setup, excel_dict):
    """
    ToDo: The sheets have to be in the right order for now, otherwise there is a foreign key validation

    Action
    -------
    1) Each dataframe is reformed into dictionary format format (excel_data_dict)
    2) excel_data_dict is declared as the query of this upload
    3) http order: post is used to upload the data
    """

    """-----------------------GENERAL----------------------"""
    for excel_item in excel_dict.items():

        # skip the timeseries table to upload it seperately
        if excel_item[0] == 'timeseries':
            continue

        table_name = 'ineed_' + excel_item[0]
        excel_data_df = excel_item[1]
        query = []

        excel_data_dict = excel_data_df.to_dict(orient='records')

        data_scenario = {
            "query": excel_data_dict
        }

        result = requests.post(
            setup['url'] + '/api/v0/schema/' + setup['schema'] + '/tables/' + table_name + '/rows/new',
            json=data_scenario,
            headers={'Authorization': 'Token %s' % setup['token']})
        print(result.json())

    print("Finished upload")


def upload_ts(setup, excel_dict):
    """
    price_path = 'timeseries_data/prices.pkl'
    irradiance_path = 'timeseries_data/PV_factors.pkl'
    temperature_path = 'timeseries_data/temperature.csv'
    irradiance_DWD_path = 'timeseries_data/irradiance_stuttgart.pkl'
    irradiance_BSRN_path = 'timeseries_data/Lindenberg2006BSRN_Irradiance_60sec.csv'
    temperature_BSRN_path = 'timeseries_data/Lindenberg2006_Temperature.csv'
    """

    for excel_idx in range(excel_dict['timeseries'].shape[0]):

        ts_data = excel_dict['timeseries'].iloc[excel_idx]
        table_name = 'ineed_timeseries'

        id = float(ts_data['id'])
        data_id = float(ts_data['data_id'])
        time_start = ts_data['time_start']
        time_end = ts_data['time_end']
        time_resolution = float(ts_data['time_resolution'])  # in minutes

        data_path = ts_data['path']
        filename, file_extension = os.path.splitext(data_path)

        keynames = ['id', 'data_id', 'value', 'time_start', 'time_end', 'time_resolution']
        upload_dict = dict.fromkeys(keynames)

        if file_extension == '.csv':

            input_data = pd.read_csv(data_path)

            # write into oep-float-string
            oep_string = '{'

            for num in range(len(input_data)):
                number = str(input_data.loc[num][0])
                if num < len(input_data) - 1:
                    oep_string = oep_string + number + ', '
                else:
                    oep_string = oep_string + number
            oep_string = oep_string + '}'

        elif file_extension == '.pkl':
            input_data = pd.read_pickle(data_path)
            # write into oep-float-string

            #
            # special case: if file has more than one column of data (e.g. irradiance_DWD
            # input_data = input_data['FG_LBERG']
            if input_data.shape[1] > 1:
                input_data = input_data['FG_LBERG']

            oep_string = '{'
            for num in range(len(input_data)):
                number = str(input_data[num])
                if num < len(input_data) - 1:
                    oep_string = oep_string + number + ', '
                else:
                    oep_string = oep_string + number

            oep_string = oep_string + '}'

        upload_dict['id'] = id
        upload_dict['data_id'] = data_id
        upload_dict['value'] = oep_string
        upload_dict['time_start'] = time_start
        upload_dict['time_end'] = time_end
        upload_dict['time_resolution'] = time_resolution

        data_scenario = {
            "query": upload_dict
        }

        result = requests.post(
            setup['url'] + '/api/v0/schema/' + setup['schema'] + '/tables/' + table_name + '/rows/new',
            json=data_scenario,
            headers={'Authorization': 'Token %s' % setup['token']})
        print(result.json())

    os.chdir('..')


def update_excel_data(schema, oep_url, OEP_TOKEN, diff_dict, id_list):
    """
    ToDo: The sheets have to be in the right order for now, otherwise there is a foreign key validation

    Action
    -------
    1) Each dataframe is reformed into dictionary format format (excel_data_dict)
    2) excel_data_dict is declared as the query of this upload
    3) http order: post is used to upload the data
    """

    """-----------------------GENERAL----------------------"""

    for diff_item in diff_dict.items():
        for id in id_list:
            table_name = 'ineed_' + diff_item[0]
            excel_data_df = diff_item[1]
            query = []

            excel_data_dict = excel_data_df.to_dict(orient='records')

            data_scenario = {
                "query": excel_data_dict
            }

            result = requests.put(
                oep_url + '/api/v0/schema/' + schema + '/tables/' + table_name + '/rows/' + str(id),
                json=data_scenario,
                headers={'Authorization': 'Token %s' % OEP_TOKEN})
            print(result.json())


def create_table(db, usecase):
    """

    Parameters
    ----------
    db: information about connection to database

    Action
    -------
    1)  metadaten_folder points into the folder where the relevant metadata is (ATTENTION: first file in folder is used)
    2)  tables_rm has all the information about the generated tables --> important;
        should be saved so it does't need to be generated every time
    3)  oem2orm_create_table --> creates tables in db
    """
    # TODO: change this to dynamic
    # reading data from metadata
    if usecase == 2:
        metadata_folder = pathlib.Path.cwd() / 'scripts' / 'Database' / 'Metadata'  # use if runme_db.py is calling
    elif usecase == 1:
        metadata_folder = pathlib.Path.cwd() / 'Metadata'  # use if use_database is calling
    else:
        raise ValueError('usecase can only be 1 or 2.')

    # get table structure from metadata
    tables_orm = oem2orm.collect_tables_from_oem(db, metadata_folder)

    # TODO:  save and reload list
    # upload empty tables to database
    result_creation = oem2orm.create_tables(db, tables_orm)
    print(result_creation)

    return tables_orm


def get_table_data(table):
    schema = 'model_draft'  # name of schema where the database is stored
    oep_url = 'https://openenergy-platform.org/'  # URL of database
    raw_result = requests.get(oep_url + '/api/v0/schema/' + schema + '/tables/' + table + '/rows/')

    result_df = pd.DataFrame(raw_result.json())
    return result_df


def get_id_diff(df1, df2):
    df_diff = pd.DataFrame(columns=df1.columns)

    for it1 in range(len(df1)):
        for it2 in range(len(df2)):

            if (df1['id'][it1] == df2['id'][it2]).all():
                break
            elif it2 + 1 == len(df2):
                df_add = pd.DataFrame(columns=df1.columns)
                df_add.loc[0] = df1.iloc[it1]
                df_diff = df_diff.append(df_add, ignore_index=True)
                print('Row from the excel file has to be added!')

    return df_diff


def get_df_diff(df1, df2, table):
    # df1: excel data
    # df2: oep data
    # compare the two dataframes row for row

    df_diff = pd.DataFrame(columns=df1.columns)
    delete_id_list = list()

    for it1 in range(len(df1)):
        for it2 in range(len(df2)):
            if (df1.iloc[it1] == df2.iloc[it2]).all():
                break
            elif it2 + 1 <= len(df2):
                # delete old row
                delete_id_list.append(df2['id'].iloc[it2])

                # insert new row later together
                df_add = pd.DataFrame(columns=df1.columns)
                df_add.loc[0] = df1.iloc[it1]
                df_diff = df_diff.append(df_add)

                print('Row from the excel file has to be added!')

    return df_diff, delete_id_list


def get_opt_config(prosumer):
    """---------------------------CONFIG-------------------------"""

    prosumer_config = {'injection_price': prosumer['injection_price'][0],
                       # 2019 # changes with size! bis10kwp: 8.16 (01.01.21), bis 40: 7.93
                       'injection/pvpeak': prosumer['injection_pvpeak'][0],
                       'gas_price': prosumer['gas_price'][0],  # 2019 #https://www.verivox.de/gas/verbraucherpreisindex/
                       'elec_price': prosumer['elec_price'][0],  # 2019 Source 79
                       'elec_emission': prosumer['elec_emission'][0],  # 2019
                       'gas_emission': prosumer['gas_emission'][0],
                       'yearly_interest': prosumer['yearly_interest'][0],  # letmathe
                       'planning_horizon': prosumer['planning_horizon'][0]}

    return prosumer_config


def get_scenario(scenario_name):
    # usecase=1 means that setup_connection() is called from use_databse.py directy
    # usecase=2 would mean that it is calles from runme_db.py
    setup = setup_connection(usecase=2)

    oep_url = setup['url']

    # get scenario
    result = requests.get(
        oep_url + "/api/v0/schema/model_draft/tables/ineed_scenario/rows/?where=scenario=" + scenario_name)
    scenario = pd.DataFrame(result.json())

    # times of scenario--> time of prosumer
    scenario_time = {'t_start': scenario['date_start'], 't_end': scenario['date_end'], 't_step': scenario['timestep']}

    # get links to prosumers
    result = requests.get(
        oep_url + "/api/v0/schema/model_draft/tables/ineed_scenario_prosumer/rows/?where=scenario_id=" + str(
            scenario['id'][0]))
    links = pd.DataFrame(result.json())
    prosumer_ids = links['prosumer_id']

    # iterate through prosumers linked to this scenario
    all_matrices = {}
    all_profiles = {}
    all_config = {}
    all_comp_dict = {}
    name_list = []

    for pro_id in prosumer_ids:
        sector_matrices, profiles, prosumer_config, comp_dict, pr_name = get_prosumer(pro_id, oep_url)
        all_matrices[pr_name] = sector_matrices
        all_profiles[pr_name] = profiles
        all_config[pr_name] = prosumer_config
        all_comp_dict[pr_name] = comp_dict
        name_list.append(pr_name)

    return all_matrices, all_profiles, all_config, scenario_time, all_comp_dict, name_list


def get_prosumer(pro_id, oep_url):
    # get topology from prosumer table
    result = requests.get(
        oep_url + "/api/v0/schema/model_draft/tables/ineed_prosumer/rows/?columns=id&?where=id=" + str(pro_id))
    prosumer = pd.DataFrame(result.json())
    prosumer_name = prosumer['prosumer_name'][0]

    # sector matrices
    sector_matrices, profiles, comp_dict = create_sector_matrix(prosumer, oep_url)

    # prosumer_config
    prosumer_config = get_opt_config(prosumer)

    return sector_matrices, profiles, prosumer_config, comp_dict, prosumer_name


def delete_all_tables(db, tables_orm):
    # ToDo: nutzbarer machen (PRIO)
    # delete tables
    oem2orm.delete_tables(db, tables_orm)


def delete_single_table(tables, table_name):
    for tab in tables:
        if tab.fullname == table_name:
            tab.drop()
            print('Dropped table ' + table_name)
            break


def delete_id(id_list, table_name, setup):
    # id: array with id's to delete
    # table: array with table names

    table_name = 'ineed_' + table_name

    # delete the row belonging to link_id
    for id in id_list:
        res = requests.delete(
            setup['url'] + '/api/v0/schema/' + setup['schema'] + '/tables/' + table_name + '/rows/?where=id=' + str(id),
            headers={'Authorization': 'Token %s' % setup['token']})
        print(res)


def delete_in_excel(table, id):
    file_name = 'Minimal_Scenario_link_neu.xlsx'
    data_path = 'Database/Data/Minimal_Scenario_link_neu.xlsx'
    temp = pd.read_excel(r'{0}'.format(data_path), sheet_name=table, engine='openpyxl')
    idx_drop = temp['link_id'][temp['link_id'] == id].index[0]
    temp.drop(idx_drop)

    """------overwrite excel---------"""
    workbook = load_workbook(data_path)
    print(workbook.sheetnames)
    writer = pd.ExcelWriter(data_path)
    del workbook[table]
    temp.to_excel(writer, sheet_name=table)
    writer.save()


def sync_excel_to_oep():
    tables_orm, oep_url, OEP_TOKEN = setup_connection()
    schema = 'model_draft'
    data_path = 'Database/Data/struktur_neu.xlsx'

    # get excel data
    dict_excel = get_excel_data(data_path)

    # get oep data
    dict_oep = dict.fromkeys(dict_excel.keys())
    comp_result = dict.fromkeys(dict_excel.keys())

    for keys in dict_excel.keys():
        table_name = 'ineed_' + keys
        result = requests.get(
            oep_url + '/api/v0/schema/' + schema + '/tables/' + table_name + '/rows/',
            headers={'Authorization': 'Token %s' % OEP_TOKEN})
        dict_oep[keys] = pd.DataFrame(result.json())

        # get differences between excel table and oeptable
        comp_result[keys], id_list = get_df_diff(dict_excel[keys], dict_oep[keys], keys)
        # comp_result[keys] = get_id_diff(dict_excel[keys], dict_oep[keys])
        update_excel_data(schema, oep_url, OEP_TOKEN, comp_result, id_list)


def create_sector_matrix(prosumer, oep_url):
    """---------------------------Matrices and Profiles------------------"""
    # get id from relevant prosumer
    prosumer_id = str(prosumer['id'][0])

    # get sizes of relevant components from prosumer_component table
    result = requests.get(
        oep_url + "/api/v0/schema/model_draft/tables/ineed_prosumer_component/rows/?where=prosumer_id=" + prosumer_id)
    comp_size = pd.DataFrame(result.json())
    comp_ids = comp_size['comp_id']

    # create list for counting the different models used in the prosumer
    # creating dict for filenames for timeseries profiules to use scripts
    model_list = []
    profiles = {}
    comp_dict = {}
    # extract relevant datapoints from component table
    for iter in range(len(comp_ids)):

        comp_id = str(comp_ids[iter])

        # data of component in df
        # ToDo: try to use query to not have to iterate
        result = requests.get(
            oep_url + "/api/v0/schema/model_draft/tables/ineed_component/rows/?where=id=" + comp_id)
        comp = pd.DataFrame(result.json())

        """----------Work off profiles first----------------"""
        if 'profile' in comp['sector'][0]:
            result = requests.get(
                oep_url + "/api/v0/schema/model_draft/tables/ineed_component_data/rows/?where=comp_id=" + comp_id)
            comp_data = pd.DataFrame(result.json())

            result = requests.get(
                oep_url + "/api/v0/schema/model_draft/tables/ineed_timeseries/rows/?where=data_id=" + str(
                    comp_data['data_id'][0]))
            ts_data = pd.DataFrame(result.json())
            profile_name = comp['comp_type'][0] + '_' + comp['model'][0]

            profiles[profile_name] = {}
            profiles[profile_name]['values'] = ts_data['value']
            profiles[profile_name]['start_date'] = ts_data['time_start']
            profiles[profile_name]['end_date'] = ts_data['time_end']
            profiles[profile_name]['resolution'] = ts_data['time_resolution']

            continue

        """---------------then create sector matrices for all other components---------------------"""
        # count how often the current model is already used in the prosumer
        model_count = model_list.count(comp['model'].iloc[[0]]) + 1

        # create name od component with system: "model" + "_1", where the number at the end
        # iterates if one more component of these model is used
        name = comp['model'][0] + '_' + str(model_count)
        df_1 = pd.DataFrame(data=[name], columns=['comp_name'])

        # create df for actual component to be sorted in sector matrix
        df_2 = comp[['comp_type', 'model']]
        df_3 = comp_size[['min_size', 'max_size', 'current_size']].iloc[[iter]]
        df_3.index = df_2.index
        df_connect = pd.concat([df_1, df_2, df_3], axis=1)

        # create list with relevant sectors for this comp
        sector_list = comp['sector'][0]

        # sort into different matrices
        if 'electricity' in sector_list:
            try:
                df_elec = df_elec.append(df_connect, ignore_index=True)
            except NameError:
                df_elec = df_connect

        if 'heat' in sector_list:
            try:
                df_heat = df_heat.append(df_connect, ignore_index=True)
            except NameError:
                df_heat = df_connect

        if 'gas' in sector_list:
            try:
                df_gas = df_gas.append(df_connect, ignore_index=True)
            except NameError:
                df_gas = df_connect

        """--------now download all data related to component-----------------"""
        result = requests.get(
            oep_url + "/api/v0/schema/model_draft/tables/ineed_component_data/rows/?where=comp_id=" + comp_id)
        comp_data = pd.DataFrame(result.json())

        data_ids = comp_data["data_id"]
        data_names = []
        data_values = []
        for data_iter in data_ids:
            result = requests.get(
                oep_url + "/api/v0/schema/model_draft/tables/ineed_data/rows/?where=id=" + str(data_iter))
            data = pd.DataFrame(result.json())

            result = requests.get(
                oep_url + "/api/v0/schema/model_draft/tables/ineed_scalar/rows/?where=data_id=" + str(data_iter))
            scalar = pd.DataFrame(result.json())

            data_names.append(data["parameter_name"][0])
            data_values.append(scalar["value"][0])

        df_properties = pd.DataFrame(data=[data_values], columns=data_names)
        comp_dict[name] = df_properties

    """---------handle flows------------"""
    sector_matrices = {}

    topo_elec = prosumer['topo_elec'][0]  # dict with all flows
    if topo_elec:
        elec_matrix = np.zeros(
            (len(df_elec['comp_name']), len(df_elec['comp_name'])))  # build quadratic matrix with numpy
        for name in df_elec['comp_name']:
            row = df_elec.index[df_elec['comp_name'] == name]
            try:
                for connection in topo_elec[name]:
                    column = df_elec.index[df_elec['comp_name'] == connection]
                    elec_matrix[row, column] = 1
            except KeyError:
                elec_matrix[df_elec.index[df_elec['comp_name'] == name], :] = 0
        df_topo_elec = pd.DataFrame(data=elec_matrix, columns=df_elec['comp_name'])
        # connect with prior dataframes
        df_elec = pd.concat([df_elec, df_topo_elec], axis=1)
        sector_matrices['electricity'] = df_elec

    topo_gas = prosumer['topo_gas'][0]  # dict with all flows
    if topo_gas:
        gas_matrix = np.zeros(len(df_gas['comp_name']))  # build quadratic matrix
        for name in df_gas['comp_name']:
            row = df_gas.index[df_gas['comp_name'] == name]
            try:
                for connection in topo_gas[name]:
                    column = df_gas.index[df_gas['comp_name'] == connection]
                    gas_matrix[row, column] = 1
            except KeyError:
                gas_matrix[df_gas.index[df_gas['comp_name'] == name], :] = 0
        df_topo_gas = pd.DataFrame(data=gas_matrix, columns=df_gas['comp_name'])
        df_gas = pd.concat([df_gas, df_topo_gas], axis=1)
        sector_matrices['gas'] = df_gas

    topo_heat = prosumer['topo_therm'][0]  # dict with all flows
    if topo_heat:
        heat_matrix = np.zeros(
            (len(df_heat['comp_name']), len(df_heat['comp_name'])))  # build quadratic matrix from all comps
        for name in df_heat['comp_name']:
            row = df_heat.index[df_heat['comp_name'] == name]
            try:
                for connection in topo_heat[name]:
                    column = df_heat.index[df_heat['comp_name'] == connection]
                    heat_matrix[row, column] = 1
            except KeyError:
                heat_matrix[df_heat.index[df_heat['comp_name'] == name], :] = 0
        df_topo_heat = pd.DataFrame(data=heat_matrix, columns=df_heat['comp_name'])
        df_heat = pd.concat([df_heat, df_topo_heat], axis=1)
        sector_matrices['heat'] = df_heat

    return sector_matrices, profiles, comp_dict


def setup_connection(usecase):
    schema = 'model_draft'  # scheme where data is stored on OEP
    oep_url = 'https://openenergy-platform.org/'  # URL of database

    # OEP data
    OEP_TOKEN = 'ab3e16480ad3aba0cb74f2d21766d4eec832946e'
    # OEP_TOKEN = '23e499ecc12586b118bd8a6e595c440952898590'  # token of jgn
    OEP_USER = 'Jonas Brucksch'
    os.environ["OEP_TOKEN"] = OEP_TOKEN
    os.environ["OEP_USER"] = OEP_USER

    # connecting do database with oem2orm
    db = oem2orm.setup_db_connection()
    # oem2orm.setup_logger()  # let you choose if logger should be active
    tables_orm = create_table(db, usecase=usecase)

    return {'tables': tables_orm, 'db': db, 'url': oep_url, 'token': OEP_TOKEN, 'schema': schema}


def config_profiles(input_profiles, t_step):
    """---------Config profiles-------------"""

    """-----------Irradiance-------------"""
    irr_values = input_profiles['Irradiance_irradiance_DWD']['values'][0]
    start_date = input_profiles['Irradiance_irradiance_DWD']['start_date'][0]
    end_date = input_profiles['Irradiance_irradiance_DWD']['end_date'][0]
    time_steps = []
    for t in pd.date_range(start_date, end_date,
                           freq='1H'):
        time_steps.append(t)
    df_irr = pd.DataFrame(data=irr_values, index=time_steps)

    # radiation data is given in J/cm^2 -> 1 J/cm^2 = 2.77778 Wh/m^2
    # we should consider this when changing to 15min resolution
    global_irradiance = df_irr[0] * 2.77778

    """-----------------Temperature-----------------"""
    start_date = input_profiles['Temperature_temp_BSRN']['start_date'][0]
    end_date = input_profiles['Temperature_temp_BSRN']['end_date'][0]
    time_steps = []
    for t in pd.date_range(start_date, end_date,
                           freq='60S'):
        time_steps.append(t)
    # important: only pass the temperature column, not the steps
    df = pd.DataFrame(data=input_profiles['Temperature_temp_BSRN']['values'][0])

    # data actually starts one hour earlier:
    main_part = df[:(60 * 8760 - 60)]
    # print(len(main_part))
    extra_hour = df[:60]
    # print(extra_hour)
    df = pd.concat([extra_hour, main_part])

    df_temp = pd.DataFrame(data=np.array(df), index=time_steps)
    temperature = df_temp.resample(str(t_step) + 'H').mean()

    """----------------Prices------------------------"""
    # prices = input_profiles['Prices_prices']['values']
    # pricedf = prices.resample(str(t_step) + 'H').mean().interpolate('pad')

    return global_irradiance, temperature[0]


def main():
    """
    --------------------------------START--------------------------
    """

    """----------------What do you want to do?-------------------------------"""
    generate_table = 0
    upload_data = 1
    upload_single_timeseries = 0

    # todo: 'delete_all/delete_one_table are risky, ask the user to confirm before deleting'
    delete_all = 0
    delete_with_id = 0
    delete_one_table = 0

    # configs for each function further down

    """---------------------------------- SETTINGS ------------------------------------ """
    # usecase=1 means that setup_connection() is called from use_databse.py directy
    # usecase=2 would mean that it is calles from runme_db.py
    setup = setup_connection(usecase=1)
    excel_data_path = 'Data/struktur_new.xlsx'

    """ -----------------------------ACTIONS---------------------------------"""
    # create
    # This function is used to generate/initialize all tables!! Not adding additional tables.
    if generate_table:
        tables_orm = create_table(setup['db'], usecase=1)

    # upload
    if upload_data:
        excel_dict = get_excel_data(excel_data_path)
        upload_excel_data(setup, excel_dict)
        upload_ts(setup, excel_dict)

    if upload_single_timeseries:
        # It's hard coded, and not valid!
        ts_id = 3
        data_id = 39
        time_start = '2019-01-01 00:00:00'
        time_end = '2019-12-31 23:59:00'
        time_resolution = 1  # in minutes
        # give the ts data path. This works if the data is can be found in the timeseries_data folder
        # if the folder is in another directory give the full path
        ts_data_path = os.path.join(os.getcwd(), 'Data', 'timeseries_data', 'Lindenberg2006_Temperature.csv')

        upload_ts(setup, ts_id, data_id, time_start, time_end, time_resolution, ts_data_path)

    # delete tables
    if delete_all:
        delete_all_tables(setup['db'], setup['tables'])

    if delete_one_table:
        table_name = 'model_draft.ineed_prosumer_component'  # name of the table to be deleted
        delete_single_table(setup['tables'], table_name)

    if delete_with_id:
        table_name = 'scenario'  # name of the table in which you want to delete something
        to_delete_ids = [2]  # ist of ids which you want to delete
        delete_id(id_list=to_delete_ids, table_name=table_name, setup=setup)


if __name__ == '__main__':
    main()
